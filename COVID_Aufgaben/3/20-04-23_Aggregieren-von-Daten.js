//Aufgabe1.1
db.subprojects.find(
{ 
    $and:[
        {theoreticalResearch:{$gt: 50}},
        {'facility.name':{$ne:"Institut für Angewandte Mathematik"}}
    ]
    
    
}
).count();


//Aufgabe1.2
db.projects.distinct("projectType");

//Aufgabe1.3
db.projects.group({
    key:{projectType:1},
    initial:{count:0},
    reduce:function(curr,result){
        result.count++;
    
    }

}  
);
//Aufgabe 2.1
db.projects.aggregate([
{$match:{
    $nor:[
        {projectType:"MANAGEMENT_PROJECT"},
        {projectType:"RESEARCH_FUNDING_PROJECT"}
        
    ]
}},
{$sort:{ title:-1}},
{$out:"projectReport1"}


]);
//Aufgabe 2.2 
db.subprojects.aggregate([
{$sort:
    {theoreticalResearch:-1}
   

   },
   {$limit:1},
   {$out:"projectReport1"}
]);

//Aufgabe 3.1

db.projects.aggregate([
 {$match:{
     $and:[
      {projectType:"REQUEST_FUNDING_PROJECT"},
      {review:{
          $gte:2,
          $lte:5
      }}
     ]
    
     
 }},
 {$addFields:{
     projectFunding:{$sum:'$fundings.amount'},
     subprojectCount:{$size:"$subprojects"}
 }},
 {$project:{
     _id:1,
     title:1,
     projectFunding:1,
     subprojectCount:1,
     subprojects:1
     
 }
 },
 //{$out:"projectReport2"}
]);

//Aufgabe 4.1
db.projects.aggregate([
    {$lookup:{
        from:"subprojects",
        localField:"subprojects._id",
        foreignField:"_id",
        as:"subprojects"
    
    }},
    {$addFields:{
        institutes:"$subprojects.facility.name"
        
    }},
    {$project:{
        _id:1,
        title:1,
        projectType:1,
        projectState:1,
        institutes:1
    }    
    },
    {$out:"projectReport3"}

]);

//Aufgabe 4.2
db.facilities.aggregate([
{$lookup:{
    from:"projects",
    localField:"projects.project_id",
    foreignField:"_id",
    as:"projects"
}},
{$project:{
    _id:1,
    name:1,
    code:1,
    
    projects:{ $filter:{
        input:"$projects",
        as:"projects",
        cond:{
            $and:[
                {isEUSponsered:false},
                {isSmallProject:false},
                {$or:[
                    {projectType:"REQUEST_FUNDING_PROJECT"},
                    {projectType:"RESEARCH_FUNDING_PROJECT"}
                ]}
            ]
        }
        
    
    }
        
    
    }   
}}
//{$out:"facilityReport1"}

]);

//Aufgabe 5.1
db.projects.aggregate([
    {$group:{
        _id:"$projectType",
        projectCount:{
            $sum:1
        },
        projectTitles:{
            $push:"$title"
        }
    }},
    {$out:"projectReport5"}
]);


//Aufgabe 5.2
db.subprojects.aggregate([

{$bucket:{
    groupBy:"$theoreticalResearch",
    boundaries:[0,51,101],
    output:{
    subprojectTitles:{
        $push:"$$ROOT.title",
        
    },
    subprojectCount:{
        $sum:1
    }
    
    
    }
    
}},
{$out:"subprojectReport3"}


]);


