//Aufgabe1.1
db.projects.find(

    {
        projectType: "REQUEST_FUNDING_PROJECT"

    }).count();

//Aufgabe 1.2
db.subprojects.aggregate([
    {
        $match: {
            appliedResearch: { $gt: 19 }

        }
    },
    {

        $group: {
            _id: "$facility",
            subprojectCount: {
                $sum: 1
            }
        }
    },
    {
        $project: {
            "_id._id": 1,
            "_id.name": 1,
            subprojectCount: 1

        }
    }
]);

//Aufgabe 2.1
db.debitors.aggregate([
    {
        $project: {
            _id: 1,
            name: 1,
            fundedProjects: {
                $size: "$fundings"
            },
            fundedAmount: {
                $sum: "$fundings.amount"
            },
            projects: "$fundings.projectName"




        }

    },
    { $out: "debitorReport" }

]);

//Aufgabe 3

db.debitors.aggregate([
    {
        $lookup: {
            from: "projects",
            let: { pid: "$fundings.project_id", debid: "$_id" },
            pipeline: [
                { $unwind: "$fundings" },
                {
                    $match: {
                        $expr: {
                            $and: [{ $in: ["$_id", "$$pid"] },
                            { $eq: ["$$debid", "$fundings._id"] }
                            ]
                        }
                    }
                },

                {
                    $project: {
                        _id: 1,
                        title: 1,
                        isFWFSponsered: 1,
                        isFFGSponsered: 1,
                        isEUSponsered: 1,
                        isSmallProject: 1,
                        type: "$projectType",
                        state: "$projectState",
                        fundings: "$fundings.amount",
                        subprojectCount: {
                            $size: "$subprojects"
                        }




                    }
                }
            ],
            as: "projects"
        }
    },

    {
        $project: {
            _id: 1,
            name: 1,
            projects: 1


        }
    },
    {$out:"debitorReport2"}



]);


