--region Übungseinheit am 14.11.2019 | Subselect 2
--1.Beispiel) Subselect
--Tabelle: C_Projects, C_FUNDING
--Berechne Sie für jedes Projekt die Fördersumme
select PROJECT_ID, sum(AMOUNT)
from C_FUNDING
group by PROJECT_ID
order by PROJECT_ID;

--Geben Sie PROJECT_ID, TITLE und FUNDING_AMOUNT aus
select cf.PROJECT_ID, sum(cf.AMOUNT) project_funding, c.TITLE
from C_PROJECTS c
         join C_FUNDING CF on c.PROJECT_ID = CF.PROJECT_ID
group by cf.PROJECT_ID, c.TITLE
order by cf.PROJECT_ID;
-----------------------------------------------------------------------------------

--1.Beispiel) Subselect
--Tabelle: C_PROJECTS, C_FUNDING
--Finden Sie das Projekt mit der hoechsten Projektfoerderung
select * from
(select max(AMOUNT) max_amount from C_FUNDING) sub;

--Geben Sie PROJECT_ID, TITLE und AMOUNT aus
select sub.max_amount, cf.PROJECT_ID, cp.TITLE
from (select max(AMOUNT) max_amount from C_FUNDING) sub
         join C_FUNDING cf on cf.AMOUNT = sub.max_amount
         join C_PROJECTS cp on cf.PROJECT_ID = cp.PROJECT_ID;
-----------------------------------------------------------------------------------

--3.Beispiel) Subselect (EIN ANDERES ÜBUNGSBLATT)
--Tabelle: C_SUBPROJECTS, C_FACILITIES
--Finden Sie das Institut (C_FACILITIES) das die meisten Subprojects umsetzt
select sub_2.FACILITY_ID, sub_1.max_subproject_count, sub_2.FACILITY_TITLE
from (select count(s.SUBPROJECT_ID) subproject_count, s.FACILITY_ID, f.FACILITY_TITLE
      from C_SUBPROJECTS s
               join C_FACILITIES f on s.FACILITY_ID = f.FACILITY_ID
      group by s.FACILITY_ID, f.FACILITY_TITLE) sub_2
         join
     (select max(count(SUBPROJECT_ID)) max_subproject_count from C_SUBPROJECTS group by FACILITY_ID) sub_1
     on sub_1.max_subproject_count = sub_2.subproject_count;
--endregion