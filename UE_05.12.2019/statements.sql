-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema Restaurant
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Restaurant
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Restaurant` DEFAULT CHARACTER SET utf8 ;
USE `Restaurant` ;

-- -----------------------------------------------------
-- Table `Restaurant`.`BRANCHES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Restaurant`.`BRANCHES` (
  `BRANCH_ID` INT NOT NULL AUTO_INCREMENT,
  `NAME` VARCHAR(30) NOT NULL,
  `DISTRICT` VARCHAR(30) NOT NULL,
  `ADDRESS` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`BRANCH_ID`),
  UNIQUE INDEX `NAME_UNIQUE` (`NAME` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Restaurant`.`GUEST_GARDENS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Restaurant`.`GUEST_GARDENS` (
  `GUEST_GARDEN_ID` INT NOT NULL AUTO_INCREMENT,
  `OPENING_TIME` DATE NOT NULL,
  `CLOSING_TIME` DATE NOT NULL,
  `BRANCHES_BRANCH_ID` INT NOT NULL,
  PRIMARY KEY (`GUEST_GARDEN_ID`),
  INDEX `fk_GUEST_GARDENS_BRANCHES_idx` (`BRANCHES_BRANCH_ID` ASC),
  CONSTRAINT `fk_GUEST_GARDENS_BRANCHES`
    FOREIGN KEY (`BRANCHES_BRANCH_ID`)
    REFERENCES `Restaurant`.`BRANCHES` (`BRANCH_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Restaurant`.`EMPLOYEES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Restaurant`.`EMPLOYEES` (
  `EMPLOYEE_ID` INT NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` VARCHAR(50) NOT NULL,
  `LAST_NAME` VARCHAR(20) NOT NULL,
  `SOCIAL_SECURITY_NUMBER` VARCHAR(10) NOT NULL,
  `BRANCHES_BRANCH_ID` INT NOT NULL,
  PRIMARY KEY (`EMPLOYEE_ID`),
  UNIQUE INDEX `SOCIAL_SECURITY_NUMBER_UNIQUE` (`SOCIAL_SECURITY_NUMBER` ASC),
  INDEX `fk_EMPLOYEES_BRANCHES1_idx` (`BRANCHES_BRANCH_ID` ASC),
  CONSTRAINT `fk_EMPLOYEES_BRANCHES1`
    FOREIGN KEY (`BRANCHES_BRANCH_ID`)
    REFERENCES `Restaurant`.`BRANCHES` (`BRANCH_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Restaurant`.`CHEFS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Restaurant`.`CHEFS` (
  `CHEF_ID` INT NOT NULL AUTO_INCREMENT,
  `EMPLOYEES_EMPLOYEE_ID` INT NOT NULL,
  PRIMARY KEY (`CHEF_ID`),
  INDEX `fk_CHEFS_EMPLOYEES1_idx` (`EMPLOYEES_EMPLOYEE_ID` ASC),
  CONSTRAINT `fk_CHEFS_EMPLOYEES1`
    FOREIGN KEY (`EMPLOYEES_EMPLOYEE_ID`)
    REFERENCES `Restaurant`.`EMPLOYEES` (`EMPLOYEE_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Restaurant`.`WAITERS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Restaurant`.`WAITERS` (
  `WAITER_ID` INT NOT NULL AUTO_INCREMENT,
  `EMPLOYEES_EMPLOYEE_ID` INT NOT NULL,
  `IS_PARTNER` INT NOT NULL,
  PRIMARY KEY (`WAITER_ID`),
  INDEX `fk_WAITERS_EMPLOYEES1_idx` (`EMPLOYEES_EMPLOYEE_ID` ASC),
  CONSTRAINT `fk_WAITERS_EMPLOYEES1`
    FOREIGN KEY (`EMPLOYEES_EMPLOYEE_ID`)
    REFERENCES `Restaurant`.`EMPLOYEES` (`EMPLOYEE_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Restaurant`.`TABLES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Restaurant`.`TABLES` (
  `TABLE_ID` INT NOT NULL AUTO_INCREMENT,
  `TABLE_NR` INT NOT NULL,
  `BRANCHES_BRANCH_ID` INT NOT NULL,
  PRIMARY KEY (`TABLE_ID`),
  INDEX `fk_TABLES_BRANCHES1_idx` (`BRANCHES_BRANCH_ID` ASC),
  CONSTRAINT `fk_TABLES_BRANCHES1`
    FOREIGN KEY (`BRANCHES_BRANCH_ID`)
    REFERENCES `Restaurant`.`BRANCHES` (`BRANCH_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Restaurant`.`WAITERS_has_TABLES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Restaurant`.`WAITERS_has_TABLES` (
  `WAITERS_WAITER_ID` INT NOT NULL,
  `TABLES_TABLE_ID` INT NOT NULL,
  PRIMARY KEY (`WAITERS_WAITER_ID`, `TABLES_TABLE_ID`),
  INDEX `fk_WAITERS_has_TABLES_TABLES1_idx` (`TABLES_TABLE_ID` ASC),
  INDEX `fk_WAITERS_has_TABLES_WAITERS1_idx` (`WAITERS_WAITER_ID` ASC),
  CONSTRAINT `fk_WAITERS_has_TABLES_WAITERS1`
    FOREIGN KEY (`WAITERS_WAITER_ID`)
    REFERENCES `Restaurant`.`WAITERS` (`WAITER_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_WAITERS_has_TABLES_TABLES1`
    FOREIGN KEY (`TABLES_TABLE_ID`)
    REFERENCES `Restaurant`.`TABLES` (`TABLE_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Restaurant`.`CHAIRS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Restaurant`.`CHAIRS` (
  `CHAIR_ID` INT NOT NULL AUTO_INCREMENT,
  `CODE` INT NOT NULL,
  `TABLES_TABLE_ID` INT NOT NULL,
  PRIMARY KEY (`CHAIR_ID`),
  UNIQUE INDEX `CODE_UNIQUE` (`CODE` ASC),
  INDEX `fk_CHAIRS_TABLES1_idx` (`TABLES_TABLE_ID` ASC),
  CONSTRAINT `fk_CHAIRS_TABLES1`
    FOREIGN KEY (`TABLES_TABLE_ID`)
    REFERENCES `Restaurant`.`TABLES` (`TABLE_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Restaurant`.`E_DISH_TYPES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Restaurant`.`E_DISH_TYPES` (
  `TYPE_ID` VARCHAR(12) NOT NULL,
  PRIMARY KEY (`TYPE_ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Restaurant`.`DISHES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Restaurant`.`DISHES` (
  `DISH_ID` INT NOT NULL,
  `NAME` VARCHAR(50) NOT NULL,
  `PRICE` INT NOT NULL,
  `E_DISH_TYPES_TYPE_ID` VARCHAR(12) NOT NULL,
  PRIMARY KEY (`DISH_ID`),
  UNIQUE INDEX `NAME_UNIQUE` (`NAME` ASC),
  INDEX `fk_DISHES_E_DISH_TYPES1_idx` (`E_DISH_TYPES_TYPE_ID` ASC),
  CONSTRAINT `fk_DISHES_E_DISH_TYPES1`
    FOREIGN KEY (`E_DISH_TYPES_TYPE_ID`)
    REFERENCES `Restaurant`.`E_DISH_TYPES` (`TYPE_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Restaurant`.`CHEFS_has_DISHES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Restaurant`.`CHEFS_has_DISHES` (
  `CHEFS_CHEF_ID` INT NOT NULL,
  `DISHES_DISH_ID` INT NOT NULL,
  PRIMARY KEY (`CHEFS_CHEF_ID`, `DISHES_DISH_ID`),
  INDEX `fk_CHEFS_has_DISHES_DISHES1_idx` (`DISHES_DISH_ID` ASC),
  INDEX `fk_CHEFS_has_DISHES_CHEFS1_idx` (`CHEFS_CHEF_ID` ASC),
  CONSTRAINT `fk_CHEFS_has_DISHES_CHEFS1`
    FOREIGN KEY (`CHEFS_CHEF_ID`)
    REFERENCES `Restaurant`.`CHEFS` (`CHEF_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CHEFS_has_DISHES_DISHES1`
    FOREIGN KEY (`DISHES_DISH_ID`)
    REFERENCES `Restaurant`.`DISHES` (`DISH_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Restaurant`.`INGREDIENTS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Restaurant`.`INGREDIENTS` (
  `INGREDIENT_ID` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`INGREDIENT_ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Restaurant`.`DISHES_has_INGREDIENTS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Restaurant`.`DISHES_has_INGREDIENTS` (
  `DISHES_DISH_ID` INT NOT NULL,
  `INGREDIENTS_INGREDIENT_ID` INT NOT NULL,
  `AMOUNT` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`DISHES_DISH_ID`, `INGREDIENTS_INGREDIENT_ID`, `AMOUNT`),
  INDEX `fk_DISHES_has_INGREDIENTS_INGREDIENTS1_idx` (`INGREDIENTS_INGREDIENT_ID` ASC),
  INDEX `fk_DISHES_has_INGREDIENTS_DISHES1_idx` (`DISHES_DISH_ID` ASC),
  CONSTRAINT `fk_DISHES_has_INGREDIENTS_DISHES1`
    FOREIGN KEY (`DISHES_DISH_ID`)
    REFERENCES `Restaurant`.`DISHES` (`DISH_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_DISHES_has_INGREDIENTS_INGREDIENTS1`
    FOREIGN KEY (`INGREDIENTS_INGREDIENT_ID`)
    REFERENCES `Restaurant`.`INGREDIENTS` (`INGREDIENT_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Restaurant`.`ORDERS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Restaurant`.`ORDERS` (
  `ORDER_ID` INT NOT NULL AUTO_INCREMENT,
  `CODE` INT NOT NULL,
  `ORDERED_AT` DATE NOT NULL,
  `WAITERS_WAITER_ID` INT NOT NULL,
  `TABLES_TABLE_ID` INT NOT NULL,
  PRIMARY KEY (`ORDER_ID`, `WAITERS_WAITER_ID`, `TABLES_TABLE_ID`),
  UNIQUE INDEX `CODE_UNIQUE` (`CODE` ASC),
  INDEX `fk_ORDERS_WAITERS1_idx` (`WAITERS_WAITER_ID` ASC),
  INDEX `fk_ORDERS_TABLES1_idx` (`TABLES_TABLE_ID` ASC),
  CONSTRAINT `fk_ORDERS_WAITERS1`
    FOREIGN KEY (`WAITERS_WAITER_ID`)
    REFERENCES `Restaurant`.`WAITERS` (`WAITER_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ORDERS_TABLES1`
    FOREIGN KEY (`TABLES_TABLE_ID`)
    REFERENCES `Restaurant`.`TABLES` (`TABLE_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Restaurant`.`DISHES_has_ORDERS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Restaurant`.`DISHES_has_ORDERS` (
  `DISHES_DISH_ID` INT NOT NULL,
  `ORDERS_ORDER_ID` INT NOT NULL,
  `AMOUNT` INT NOT NULL,
  PRIMARY KEY (`DISHES_DISH_ID`, `ORDERS_ORDER_ID`, `AMOUNT`),
  INDEX `fk_DISHES_has_ORDERS_ORDERS1_idx` (`ORDERS_ORDER_ID` ASC),
  INDEX `fk_DISHES_has_ORDERS_DISHES1_idx` (`DISHES_DISH_ID` ASC),
  CONSTRAINT `fk_DISHES_has_ORDERS_DISHES1`
    FOREIGN KEY (`DISHES_DISH_ID`)
    REFERENCES `Restaurant`.`DISHES` (`DISH_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_DISHES_has_ORDERS_ORDERS1`
    FOREIGN KEY (`ORDERS_ORDER_ID`)
    REFERENCES `Restaurant`.`ORDERS` (`ORDER_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;