//UE: Seite 25

db.getCollection("projects").find({
    'fundings.debitorName': "Sun Microsystems inc."
})
//Aufgabe1
db.projects.updateMany({},

    {
        $rename: {
            isFFGSponsered: "isFFGSponsored",
            isFWFSponsered: "isFFWSponsored",
            isEUSponsered: "isEUSponsored"
        },
        $set: {
            isSmallProject: false,
            reviews: [3, 5, 1, 3],
            update: true,
        }


    });
//Aufgabe 2
db.projects.updateMany(
    {
        $and: [
            { projectType: "MANAGEMENT_PROJECT" },
            { $where: "this.subprojects.length > 1" }
        ]
    },
    {
        $set: {
            projectState: "CREATED"
        }
    });

//Aufgabe 3

db.projects.updateMany(
    {
        'fundings.debitorName': "Sun Microsystems inc."
    },
    {
        $set: {
            isFFGSponsored: false,
            isFFWSponsored: false
        },
        $unset: {
            update: 1
        },
        $push: {
            fundings: {
                $each:
                    [
                        { _id: new ObjectId("874632936283294250329311"), debitorName: "TU Wien", amount: NumberLong("20000") },
                        { _id: new ObjectId("874632936283294250329321"), debitorName: "TU Berlin", amount: NumberLong("20000") }
                    ]
            }
        }

    }
);

