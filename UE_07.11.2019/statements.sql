--region Übungseinheit am 07.11.2019 | Subselect 1
--1. Beispiel) Subselect
--Tabelle: EMPLOYEES
--Finden Sie den Angestellten mit dem höchsten Einkommen
select LAST_NAME, FIRST_NAME, SALARY
from EMPLOYEES
where SALARY = (select max(SALARY) from EMPLOYEES);

--Finden Sie den Angestellten der am längsten im Unternehmen angestellt ist
select LAST_NAME, FIRST_NAME, HIRE_DATE
from EMPLOYEES
where HIRE_DATE = (select max(HIRE_DATE) from EMPLOYEES);
-----------------------------------------------------------------------------------

--2. Beispiel) Subselect
--Tabelle: EMPLOYEES
--Ermitteln Sie wieviele Mitarbeiter die Abteilung mit den meisten Mitarbeitern hat
(select max(count(EMPLOYEE_ID)) from EMPLOYEES group by DEPARTMENT_ID);

--Geben Sie die ID der Abteilung und die Anzahl der Mitarbeiter an.
select DEPARTMENT_ID, count(EMPLOYEE_ID) Mitarbeiteranzahl
from EMPLOYEES e
having count(EMPLOYEE_ID) = (select max(count(EMPLOYEE_ID)) from EMPLOYEES group by DEPARTMENT_ID)
group by DEPARTMENT_ID;
-----------------------------------------------------------------------------------

--3. Beispiel) Subselect
--Tabelle: EMPLOYEES
--Ermitteln Sie für jede Abteilung den Mitarbeiter mit dem höchsten Einkommen
select FIRST_NAME, LAST_NAME, DEPARTMENT_ID
from EMPLOYEES
where (SALARY, DEPARTMENT_ID) in (select max(SALARY), DEPARTMENT_ID from EMPLOYEES group by DEPARTMENT_ID)
order by DEPARTMENT_ID;

--Geben Sie die ID der Abteilung, den Vornamen und Nachnamen des Mitarbeirs und sein Einkommen an
select DEPARTMENT_ID, FIRST_NAME, LAST_NAME, SALARY
from EMPLOYEES
where (SALARY, DEPARTMENT_ID) in (select max(SALARY), DEPARTMENT_ID from EMPLOYEES group by DEPARTMENT_ID)
order by DEPARTMENT_ID;
-----------------------------------------------------------------------------------

--4. Beispiel) Subselect
--Tabelle: DUAL, SCHEDULE, EMPLOYEES
--Generieren Sie für den Dezember 2019 alle möglichen Datumswerte
select E.LAST_NAME, E.FIRST_NAME, sub1.date_num, s.DESCRIPTION
from (select TO_DATE('30.11.2019', 'dd.MM.yyyy') + level date_num
      from DUAL
      connect by level <= 31) sub1
         left join scedule s
                   on extract(day from s.AT_DATE) = extract(day from sub1.date_num) and
                      extract(month from s.AT_DATE) = extract(month from sub1.date_num) and
                      extract(year from s.AT_DATE) = extract(year from sub1.date_num)
         left join EMPLOYEES E on s.EMPLOYEE_ID = E.EMPLOYEE_ID
order by sub1.date_num;
--endregion 1