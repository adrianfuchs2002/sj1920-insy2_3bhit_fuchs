// Student Schema
db.createCollection("student", {
    validationLevel: "strict",
    validationAction: "error",
    validator: {
        $jsonSchema: {
            bsonType: "object",
            required: [
                "_id", "firstName", "lastName", "birthday",
                "gender", "contact", "address", "career"
            ],
            additionalProperties: true,
            properties: {
                _id: {
                    bsonType: "objectId",
                    maxLength: 50
                },
                firstName: {
                    bsonType: "string",
                    maxLength: 50
                },
                lastName: {
                    bsonType: "string"
                },
                birthday: {
                    bsonType: "date"
                },
                gender: {
                    enum: [
                        "MALE", "FEMALE"
                    ]
                },
                contact: {
                    bsonType: "array",
                    items: {
                        bsonType: "object",
                        required: ["phone", "email"],
                        properties: {
                            phone: {
                                bsonType: "string",
                                maxLength: 50
                            },
                            email: {
                                bsonType: "string"
                            }
                        }
                    }
                },
                address: {
                    bsonType: "array",
                    items: {
                        bsonType: "object",
                        required: ["country", "postalCode", "location", "street"],
                        properties: {
                            country: {
                                bsonType: "string"
                            },
                            postalCode: {
                                bsonType: "string"
                            },
                            location: {
                                bsonType: "string"
                            },
                            street: {
                                bsonType: "string"
                            }
                        }
                    }
                },
                career: {
                    bsonType: "array",
                    items: {
                        bsonType: "object",
                        required: ["level", "duration"],
                        properties: {
                            level: {
                                enum: [
                                    "KINDERGARTEN", "PRIMARY_SCHOOL", "SECONDARY_SCHOOL",
                                    "TECHNICAL_SCHOOL", "MATURA", "COLLEGE", "UNIVERSITY",
                                    "CHARTERED_ENGINEER", "DIPLOMA"
                                ]
                            },
                            duration: {
                                bsonType: "int"
                            }
                        }
                    }
                }
            }
        }
    }
}
);


// Student Datensätze
db.students.insertMany([{
    _id: ObjectId("874632936283294250329114"),
    firstName: "Alexander",
    lastName: "Kauer",
    birthday: new Date('Jan 01, 2001'),
    gender: "MALE",
    contact: {
        phone: "03234/434234",
        email: "a.kauer@htlkrems.at"
    },
    address: {
        country: "Austria",
        postalCode: "3540",
        location: "Krems a. d. Donau",
        street: "Wienerstrasse 12"
    },
    career: [{
        level: "KINDERGARTEN",
        duration: NumberInt(4)
    }, {
        level: "PRIMARY_SCHOOL",
        duration: NumberInt(4)
    }, {
        level: "TECHNICAL_SCHOOL",
        duration: NumberInt(5)
    }]
}, {
    _id: ObjectId("874632936283294250329115"),
    firstName: "Christian Karl",
    lastName: "Lauer",
    birthday: new Date('Mar 12, 1999'),
    gender: "MALE",
    contact: {
        phone: "03234/434234",
        email: "c.lauer@htlkrems.at"
    },
    address: {
        country: "Austria",
        postalCode: "3540",
        location: "Krems a. d. Donau",
        street: "Schuettgasse 21"
    },
    career: [{
        level: "KINDERGARTEN",
        duration: NumberInt(1)
    }, {
        level: "PRIMARY_SCHOOL",
        duration: NumberInt(4)
    }, {
        level: "TECHNICAL_SCHOOL",
        duration: NumberInt(5)
    }]
}, {
    _id: ObjectId("874632936283294250329116"),
    firstName: "Martin",
    lastName: "Weidenauer",
    birthday: new Date('Mar 21, 2001'),
    gender: "MALE",
    contact: {
        phone: "03234/434234",
        email: "m.weidenauer@htlkrems.at"
    },
    address: {
        country: "Austria",
        postalCode: "3472",
        location: "Melk",
        street: "Hauptstrasse 10"
    },
    career: [{
        level: "PRIMARY_SCHOOL",
        duration: NumberInt(4)
    }, {
        level: "TECHNICAL_SCHOOL",
        duration: NumberInt(5)
    }]
}, {
    _id: ObjectId("874632936283294250329117"),
    firstName: "Lukas",
    lastName: "Aufmesser",
    birthday: new Date('Mar 12, 2000'),
    gender: "MALE",
    contact: {
        phone: "03234/434234",
        email: "l.aufmesser@htlkrems.at"
    },
    address: {
        country: "Austria",
        postalCode: "3540",
        location: "Krems a. d. Donau",
        street: "Pulverturmstrasse 7"
    },
    career: [{
        level: "KINDERGARTEN",
        duration: NumberInt(2)
    }, {
        level: "PRIMARY_SCHOOL",
        duration: NumberInt(4)
    }, {
        level: "TECHNICAL_SCHOOL",
        duration: NumberInt(5)
    }]
}
]);


// Students Beispiele
// Alle männlichen Schueler aus Krems und alle weiblichen Schueler aus Melk ausgeben
// Absteigend nach Nach- und Vornamen sortiert
// Nachname, Vorname, Kontaktinformation wird ausgegeben
// nur 5 Ergebnisse ausgeben
db.getCollection("students").find(
    {
        $or: [
            {
                $and: [
                    { gender: { $eq: "MALE" } },
                    { "this.address.postalCode": { $eq: "3540" } }
                ]
            },
            {
                $and: [
                    { gender: { $eq: "FEMALE" } },
                    { "this.address.postalCode": { $eq: "3472" } }
                ]
            }
        ]
    },
    {
        lastName: true, firstName: true, contact: true
    }
).sort({ lastName: -1, firstName: -1 }).limit(5);


// Fuer Schueler mit mehr als 2 Bildungsstufen "MATURA"
// mit der duration 1 hinzufügen
// nur hinzufügen, wenn sie nicht vorhanden ist
db.getCollection("students").updateMany(
    {
        $where: "this.career.length >= 2"
    },
    {
        $addToSet: {
            career: {
                level: "MATURA",
                duration: NumberInt(1)
            }
        }
    }
);


// alle mit "TECHNICAL_SCHOOL", duration: 5
// und "MATURA", duration: 0 
// sollen um folgendes erweitert werden:
// "CHARTERED_ENGINEER", duration: 0
db.getCollection("students").updateMany(
    {
        $and: [
            {
                $and: [
                    { "this.career.level": "TECHNICAL_SCHOOL" },
                    { "this.career.duration": { $eq: NumberInt(5) } }
                ]
            },
            {
                $and: [
                    { "this.career.level": "MATURA" },
                    { "this.career.duration": { $eq: NumberInt(0) } }
                ]
            }
        ]
    },
    {
        $addToSet: {
            career: {
                level: "CHARTERED_ENGINEER",
                duration: NumberInt(0)
            }
        }
    }
);   