--region Aufgabenblatt - Funktionen (UE_10.10.2019)
--1. Beispiel) Stringfunktionen
--Tabelle: EMPLOYEES
--1.1:
select EMPLOYEE_ID, LAST_NAME
from EMPLOYEES
where upper(LAST_NAME)='KING';
--1.2:
select EMPLOYEE_ID, LAST_NAME, FIRST_NAME
from EMPLOYEES
where upper(LAST_NAME)='KING';
--1.3:
select EMPLOYEE_ID, LAST_NAME, FIRST_NAME
from EMPLOYEES
where upper(LAST_NAME)='KING'
order by LAST_NAME asc;

--Tabelle: L_Projects
--1.1:
select TITLE
from L_PROJECTS
where length(TITLE) > 20;
--1.2:
select TITLE, PROJECT_ID, PROJECT_TYPE
from L_PROJECTS
where length(TITLE) > 20;
--1.3:
select TITLE, PROJECT_ID, PROJECT_TYPE
from L_PROJECTS
where length(TITLE) > 20
order by TITLE;
--1.4:
select TITLE, PROJECT_ID, PROJECT_TYPE
from L_PROJECTS
where length(TITLE) > 20
order by TITLE
offset 1 row
fetch first 2 rows only;

--Tabelle: Debitor_Report
--1.1:
select *
from DEBITOR_REPORT;
--1.2:
select replace(FACILITY_TITLE, 'Institut für',' ') as "Facility_Title"
from DEBITOR_REPORT;
--1.3:
select replace(FACILITY_TITLE, 'Institut für',' ') as "Facility_Title"
from DEBITOR_REPORT
order by FACILITY_TITLE;

--2. Beispiel) Datumsfunktionen
--Theorie dazu eigenständig am Schluss der Übung erarbeiten, da wir dieses Thema in der Vorlesung noch nicht besprochen haben
--endregion