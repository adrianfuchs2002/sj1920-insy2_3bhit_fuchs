//UE: Seite 24

db.getCollection("projects").find({});

db.getCollection("projects").find({}, {
    'fundings.amount': 1
});
//Aufgabe 1
db.getCollection("projects").find(
    {
        $or: [
            {
                $and: [
                    { projectType: "REQUEST_FUNDING_PROJECT" },
                    { isFFWSponsored: true }
                ]
            },
            {
                $and: [
                    { projectType: "RESEARCH_FUNDING_PROJECT" },
                    {
                        $or: [
                            { isEUSponsored: true },
                            { isFFGSponsored: true }
                        ]
                    }
                ]
            }
        ]
    },
    {
        title: 1,
        projectType: 1,
        isFFGSponsored: 1,
        isFFWSponsored: 1,
        isEUSponsored: 1

    }

).sort({ title: 1 });
//Aufgabe 2
db.getCollection("projects").find(
    {
        $and:
            [
                {
                    $where: function () {
                        return this.subprojects.length == 2 && this.fundings.length == 2;
                    }
                },
                { projectType: "MANAGEMENT_PROJECT" }
            ]
    },
    {
        title: 1,
        projectType: 1,
        projectState: 1
    }
);

//Aufgabe 3

db.getCollection("projects").find(
    {
        $and: [
            { 'fundings.amount': { $gt: 100000 } },
            { 'fundings.debitorName': "Oracle Microsystems inc." },
            { isEUSponsored: true },
        ]
    },
    {
        title: 1,
        projectType: 1,
        projectstate: 1,
        fundings: 1
    });

//Aufgabe 4
db.getCollection("subprojects").find({});
db.getCollection("subprojects").find(
    {
        $and: [
            {
                $where: function () {
                    return this.appliedResearch + this.theoreticalResearch + this.focusResearch == 100;
                }
            },
            { 'facility.name': "Institut für Softwareentwicklung" }
        ]
    },
    {
        _id: 1,
        title: 1
    });


